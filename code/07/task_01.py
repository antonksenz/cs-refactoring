import math
import sys

def get_triangle_sides():
    try:
        a = float(input("Введіть довжину першої сторони: "))
        b = float(input("Введіть довжину другої сторони: "))
        c = float(input("Введіть довжину третьої сторони: "))
        return a, b, c
    except ValueError:
        print("Некоректні дані. Введіть числові значення для довжин сторін.")
        sys.exit(1)

def is_valid_triangle(a, b, c):
    return a + b > c and a + c > b and b + c > a

def calculate_triangle_area(a, b, c):
    p = (a + b + c) / 2
    return math.sqrt(p * (p - a) * (p - b) * (p - c))

def main():
    a, b, c = get_triangle_sides()

    if is_valid_triangle(a, b, c):
        area = calculate_triangle_area(a, b, c)
        print(f"Площа трикутника: {area}")
        sys.exit(0)
    else:
        print("Трикутник з такими сторонами не існує.")
        sys.exit(1)

if __name__ == "__main__":
    main()
