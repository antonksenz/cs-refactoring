import unittest
from unittest.mock import patch
from io import StringIO
import task_01_main

class TestTriangleAreaCalculator(unittest.TestCase):

    @patch('builtins.input', side_effect=['3', '4', '5'])
    def test_valid_triangle(self, mock_input):
        with patch('sys.stdout', new_callable=StringIO) as mock_stdout:
            task_01_main.main()
            result = mock_stdout.getvalue().strip()
            self.assertIn("Площа трикутника: 6.0", result)

    @patch('builtins.input', side_effect=['1', '2', '3'])
    def test_invalid_triangle(self, mock_input):
        with patch('sys.stdout', new_callable=StringIO) as mock_stdout:
            task_01_main.main()
            result = mock_stdout.getvalue().strip()
            self.assertIn("Трикутник з такими сторонами не існує.", result)

    @patch('builtins.input', side_effect=['a', '2', '3'])
    def test_invalid_input(self, mock_input):
        with patch('sys.stdout', new_callable=StringIO) as mock_stdout:
            task_01_main.main()
            result = mock_stdout.getvalue().strip()
            self.assertIn("Некоректні дані. Введіть числові значення для довжин сторін.", result)

    @patch('builtins.input', side_effect=['3', '4', '10'])
    def test_invalid_triangle_condition(self, mock_input):
        with patch('sys.stdout', new_callable=StringIO) as mock_stdout:
            task_01_main.main()
            result = mock_stdout.getvalue().strip()
            self.assertIn("Трикутник з такими сторонами не існує.", result)

if __name__ == '__main__':
    unittest.main()
