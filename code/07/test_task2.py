import math
import sys
import unittest
from task_02_main import main
class Calculator:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def calculate_z1(self):
        denominator = 1 + math.atan(self.y)
        if denominator != 0:
            return math.tan(self.x) / denominator
        else:
            raise ValueError("Некоректні дані. Знаменник не може дорівнювати нулю.")

    def calculate_z2(self):
        denominator = math.sin(self.y + self.x)
        if denominator != 0:
            return math.cos(self.y + self.x) - self.x / denominator
        else:
            raise ValueError("Некоректні дані. Знаменник не може дорівнювати нулю.")

    def calculate_z3(self):
        try:
            z1 = self.calculate_z1()
            z2 = self.calculate_z2()
            return math.exp(z1 + z2) * (z1 / z2)
        except ValueError as e:
            raise ValueError(f"Некоректні дані: {str(e)}")

class TestCalculator(unittest.TestCase):

    def test_calculate_z1(self):
        calculator = Calculator(x=1, y=2)
        result = calculator.calculate_z1()
        self.assertAlmostEqual(result, 0.739, places=3)  

    def test_calculate_z2(self):
        calculator = Calculator(x=1, y=2)
        result = calculator.calculate_z2()
        self.assertAlmostEqual(result, -8.076, places=3)  

    def test_calculate_z3(self):
        calculator = Calculator(x=1, y=2)
        result = calculator.calculate_z3()
        self.assertAlmostEqual(result, -5.957e-05, places=3) 

def main():
    try:
        x = float(input("Введіть значення x: "))
        y = float(input("Введіть значення y: "))
        
        calculator = Calculator(x, y)
        z1 = calculator.calculate_z1()
        z2 = calculator.calculate_z2()
        z3 = calculator.calculate_z3()

        print(f"Z1 = {z1}")
        print(f"Z2 = {z2}")
        print(f"Z3 = {z3}")

    except ValueError as e:
        print(f"Некоректні дані: {str(e)}")
        sys.exit(1)

if __name__ == "__main__":
    unittest.main()  
    main()
